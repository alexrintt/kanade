<img src="https://user-images.githubusercontent.com/51419598/202943201-f15a7fb0-4195-4723-9bad-3e13ac0d26ea.svg" />

<sup>Banner and icon by [@WSTxda](https://linktr.ee/wstxda).</sup>

## Kanade Apk Extractor

<a href="https://github.com/alexrintt/kanade/releases">
  <p>
    <img src="https://img.shields.io/github/v/release/alexrintt/kanade?label=latest&style=flat-square">
    <img src="https://img.shields.io/github/downloads/alexrintt/kanade/total?color=000t&style=flat-square">
    <img src="https://img.shields.io/badge/compatibility-4.4+-orange?style=flat-square">
    <img src="https://img.shields.io/badge/API-19+-green?style=flat-square">
  </p>
</a>

Kanade is a simple app that allows you to extract apk from your apps list. Fully open-source, zero-ads and no-tracking.

## How it looks

- Multiple themes.
- Multiple fonts.
- English, Japanese, Portuguese, German and Spanish supported.
- Minimal UI.

<!--

PlayStore link, add again when the new version is available.

<p align="center">
  <a href="https://play.google.com/store/apps/details?id=io.alexrintt.kanade"><img height="80" alt="Get it on Google Play" src="https://user-images.githubusercontent.com/51419598/170156499-fc45733a-2701-4386-be72-f28181c87cf0.png"/></a>
</p>

-->

<div>
  <kbd><img src="https://user-images.githubusercontent.com/51419598/201553663-07ad5cb4-6745-4b40-bbf4-cbd0998fdea2.jpg" width="200"></kbd>
  <kbd><img src="https://user-images.githubusercontent.com/51419598/201553659-bd05160f-a869-4387-8b9c-150672dbebb8.jpg" width="200"></kbd>
  <kbd><img src="https://user-images.githubusercontent.com/51419598/201553667-f20e6ee3-df40-4ff7-b409-00b8f58a46eb.jpg" width="200"></kbd>
</div>

<br />

<details>
  <summary>See full screenshots</summary>

<div align="center">
  <kbd><img src="https://user-images.githubusercontent.com/51419598/201553717-5bd941db-4320-4301-bfdf-dbde2b890f4f.jpg" width="200"></kbd>
  <kbd><img src="https://user-images.githubusercontent.com/51419598/201553719-0237de04-0307-425d-bd8f-eb9ac3ef3dcb.jpg" width="200"></kbd>
  <kbd><img src="https://user-images.githubusercontent.com/51419598/201553716-6669955e-5e5b-4955-9536-2e63eb379448.jpg" width="200"></kbd>
  <kbd><img src="https://user-images.githubusercontent.com/51419598/201553714-a9af76c4-7e18-4279-9627-5d367ac2b7e8.jpg" width="200"></kbd>
</div>

<div align="center">
  <kbd><img src="https://user-images.githubusercontent.com/51419598/201553570-88614248-6fb4-42e2-9c0f-f43b771011bf.jpg" width="200"></kbd>
  <kbd><img src="https://user-images.githubusercontent.com/51419598/201553562-a07ff75b-e3d4-4a94-9340-9f1ffae3f4f4.jpg" width="200"></kbd>
  <kbd><img src="https://user-images.githubusercontent.com/51419598/201553568-7f928924-5840-4a3b-8b45-54c8984b6b84.jpg" width="200"></kbd>
  <kbd><img src="https://user-images.githubusercontent.com/51419598/201553565-a37fbd94-823b-4a06-83a6-487f99af812d.jpg" width="200"></kbd>
</div>

<div align="center">
  <kbd><img src="https://user-images.githubusercontent.com/51419598/201553663-07ad5cb4-6745-4b40-bbf4-cbd0998fdea2.jpg" width="200"></kbd>
  <kbd><img src="https://user-images.githubusercontent.com/51419598/201553659-bd05160f-a869-4387-8b9c-150672dbebb8.jpg" width="200"></kbd>
  <kbd><img src="https://user-images.githubusercontent.com/51419598/201553667-f20e6ee3-df40-4ff7-b409-00b8f58a46eb.jpg" width="200"></kbd>
  <kbd><img src="https://user-images.githubusercontent.com/51419598/201553669-44120285-7f3a-4cb8-a85e-3c5beb2b16c4.jpg" width="200"></kbd>
</div>

<div align="center">
  <kbd><img src="https://user-images.githubusercontent.com/51419598/201552690-cd442f03-c924-4a31-a776-5f9a84d1bf61.jpg" width="200" /></kbd>
  <kbd><img src="https://user-images.githubusercontent.com/51419598/201552685-44fb916e-c863-41da-9f9c-7eeb8cda70d4.jpg" width="200" /></kbd>
  <kbd><img src="https://user-images.githubusercontent.com/51419598/201552687-46b71a62-6b89-492e-8280-371ae879eb00.jpg" width="200" /></kbd>
  <kbd><img src="https://user-images.githubusercontent.com/51419598/201552689-95bbfd82-8f26-41cd-98c2-46986a6affa6.jpg" width="200" /></kbd>
</div>
  
</details>

---

<div>
  <a href="https://apt.izzysoft.de/fdroid/index/apk/io.alexrintt.kanade">
    <img height="80" src="https://user-images.githubusercontent.com/51419598/202963424-371af9f5-e433-4f23-8cd0-537fe6fc013f.png">
  </a>
  <a href="https://github.com/alexrintt/kanade/releases">
    <img height="80" src="https://user-images.githubusercontent.com/51419598/202963419-6095ee98-88a5-486f-9c84-a0bd2d8c700e.png">
  </a>
</div>

## What's an apk extractor

Installed apps from PlayStore doesn't expose their apk installation files by default. So if you want to share some app you will need to send the PlayStore link, and if you are offline or the app is no longer available on the PlayStore you won't be able to do so.

So here we are, whatever the reason, if you want to share some app directly through a p2p connection (Bluetooth, Wifi-Direct, etc.) you can use apk extractors! These kind of apps allow the user to extract the hidden apk files from almost any installed app to a visible location (e.g Downloads folder).

## Features

This is what is currently available:

- Apk extraction, I'm sure you are not expecting that.
- Multiple apk extraction at once.
- Select/deselect all.
- List and search device apps (and internal packages).
- Extract to a desired location, no security or privacy issues brought by `MANAGE_EXTERNAL_STORAGE`.
- Multiple color themes and fonts.
- ~~Bad designer~~ Minimalist UI.
- No ads or tracking, this app doesn't even has `INTERNET` permission.

## Missing features

Listed by priority:

- List extracted apks files inside the app (A).
- Allow uninstall apps (B).
- Share listed apk through bluetooth or wifi-direct (C, requires A).
- Analyze apk metainfo (name, version, package, etc.) (D).
- Sign-in apk (E).

## Installation (Universal apk)

The app universal apk (20MB) is delivered on these fonts:

- ([Repository Releases](https://github.com/alexrintt/kanade/releases)) Most easy one-time way to install: download the universal apk (20MB) directly at [github.com/alexrintt/kanade/releases](https://github.com/alexrintt/kanade/releases).

- ([IzzyOnDroid](https://apt.izzysoft.de/fdroid/index/apk/io.alexrintt.kanade)) You can also download the apk directly at [apt.izzysoft.de/fdroid/index/apk/io.alexrintt.kanade](https://apt.izzysoft.de/fdroid/index/apk/io.alexrintt.kanade) (F-Droid based repository).

- ([F-Droid + IzzyOnDroid](https://f-droid.org/packages/io.alexrintt.kanade)) Most easy way to install and get updates: [f-droid.org/packages/io.alexrintt.kanade](https://f-droid.org/packages/io.alexrintt.kanade). This app is indexed by [IzzyOnDroid repository](https://gitlab.com/IzzyOnDroid/repo) (F-Droid subrepository). **If it's your first time using IzzyOnDroid or F-Droid, you will need to follow these instructions:**

  - Install [F-Droid client](https://f-droid.org/F-Droid.apk).
  - On F-Droid client go to **Settings** -> **Repositories** -> **Add new repository** (Plus icon at top right).
  - Type `https://apt.izzysoft.de/fdroid/repo` then click **Add**.
  - You can now click on this link to directly go to the app installation page: [f-droid.org/packages/io.alexrintt.kanade](https://f-droid.org/packages/io.alexrintt.kanade).

## Installation (x86-64, armeabi-v7 and arm64-v8)

Specific-abi apks are compatible only with a subset of devices but they are substantially lighter than the universal apk. So, if you are that guy from _The Matrix_ who stares at falling green cliffs all day, feel free to download and install your specific-apk (8MB) at:

- ([Repository Releases](https://github.com/alexrintt/kanade/releases)) Most easy one-time way to install: [github.com/alexrintt/kanade/releases](https://github.com/alexrintt/kanade/releases).

## How it works

To display all installed apps the [`🔗 device_apps`](https://pub.dev/packages/device_apps) package is used and the apk extraction (that is a simple copy/paste operation between two files) is possible by [`🔗 shared_storage`](https://pub.dev/packages/shared_storage) package.

## Build by your own

Flutter has a [great documentation](https://docs.flutter.dev/get-started/install) in case you don't have a configured Flutter environment yet.

### 1. Get deps and generate l10n local library

To get app dependencies:

```shell
flutter pub get
```

To run the code generation (that generates the `flutter_gen` library used for i18n features):

```shell
flutter gen-l10n
```

### 2. Generate binaries

If you're looking for the apk:

```shell
flutter build apk
# or specific-abis
flutter build apk --split-per-abi
```

You can also generate the app bundle:

```shell
flutter build appbundle
```

## Contributing

There are several ways to contribute:

- To improve the translation: 1. open the `/i18n` folder, 2. create a file `app_<thelangcodeyouwanttoaddorimprove>.arb` and 3. translate the keys 4. then open a PR, that's it.

- To report a bug, create a new issue with an screenshot or a small description of the bug.

- To request a feature please add an issue to further discuss.

If you wanna contribute in private, you can also ping me on my email [reach@alexrintt.io](mailto://reach@alexrintt.io) to discuss any of the points above.

Thanks in advance.

## Contributors

- Glad to have your work here [@WSTxda](https://linktr.ee/wstxda)! Thanks for your efforts on the [app icon](https://t.me/WSTprojects/1401), banner and the download badges.

- I wanna thanks [IzzySoft](https://gitlab.com/IzzySoft) for taking time to point the [version code issue #13](https://github.com/alexrintt/kanade/issues/13), indexing this app to the [IzzyOnDroid app repository](https://gitlab.com/IzzyOnDroid/repo) and [helping with fastlane docs #15](https://github.com/alexrintt/kanade/pull/15), thanks man!

## Credits

- Credits to all open-source libraries used in this app, see full list on [pubspec.yaml](/pubspec.yaml).
- All fonts in this repository are subject to copyright of it's respective owners.
- All images in this repository are subject to copyright of it's respective owners.

For content removal, please, ping me on my email [reach@alexrintt.io](mailto:reach@alexrintt.io).

<br />

<samp>

<h2 align="center">
  Open Source
</h2>
<p align="center">
  <sub>Copyright © 2021-present, Alex Rintt.</sub>
</p>
<p align="center">Kanade <a href="https://github.com/alexrintt/kanade/blob/master/LICENSE">is MIT licensed 💖</a></p>
<p align="center">
  <img src="https://user-images.githubusercontent.com/51419598/138740064-92e4c38a-e648-41b5-8432-da0962028f62.png" width="50" />
</p>

</samp>
